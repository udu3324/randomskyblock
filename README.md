# RandomSkyBlock

Gives players a random item (including blocks) on an interval.

## Permissions

- `randomskyblock.control` - Required to control the plugin with commands.

## Commands

- `rsb <on|off>` - Enable or disable the random items.
- `rsb interval <ticks>` - Change the interval of random items.

## Default configuration

```yaml
# Whether or not items will be given
enabled: false

# How often (in seconds) to give items
interval: 45

# Whether to play a sound when an item is given
sound: true
```
