package net.robiotic.randomskyblock;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class ItemRandomizer extends BukkitRunnable {
    private boolean sound;
    private JavaPlugin plugin;
    private Random random;

    ItemRandomizer(JavaPlugin plugin, boolean sound) {
        this.plugin = plugin;
        this.sound = sound;
        this.random = new Random();
    }

    @Override
    public void run() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            Material material = Material.values()[random.nextInt(Material.values().length)];
            ItemStack stack = new ItemStack(material);
            player.getInventory().addItem(stack);

            if (sound) {
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 10, 2);
            }
        }
    }
}
